# Probability and Statistics

This is a summary of the Probability and Statistics lectures held at the ETH
Zürich in the summer semester of 2019 by Dr. F. Balabdaoui.

For the full set of notes see `ps-summary.pdf`. For individual chapters, see
`chapters`. The chapter numbering is based on the *Fundamentals of Probability:
A First Course* by Anirban DasGupta.

As this is a summary, there are **no** proofs or examples in this document.

## Disclaimer

I cannot guarantee completeness, nor correctness either due to time constraints
or knowledge gaps. These notes are mainly for my own understanding and a way to
share them along with the source in a practical way.

Do not hesitate to send me an email if you discover any mistakes, ranging over
mathematical errors, spelling mistakes, grammatical or typesetting issues - I'm
open to any suggestions.

## Compiling yourself

These notes are compiled with and written in the `pandoc` markdown flavour. To
compile the pdfs yourself, install [pandoc](https://pandoc.org/) and call

`pandoc source/*md -o ps-summary.pdf`

Most *nix distros will have it available in the regular repositories. `pandoc`
is also available in OSX via [homebrew](https://brew.sh). For more help
installing see [the pandoc install guides](https://pandoc.org/installing.html).
There is also an [online](https://pandoc.org/try/) version that doesn't require
installing any tools.

There are many more examples and demos on the `pandoc`
[website](https://pandoc.org/demos.html).
