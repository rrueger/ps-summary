---
geometry: margin=1in
header-includes: |
    \usepackage{dsfont}
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \renewcommand{\footrulewidth}{0.4pt}
    \fancyhead[CO,CE]{ETH Z\"urich - Probability and Statistics - Dr. F. Balabdaoui}
    \fancyhead[RO,RE]{}
    \fancyhead[LO,LE]{}
    \fancyfoot[CO,CE]{\href{https://gitlab.ethz.ch/rrueger/ps-summary}{git.ethz.ch/rrueger/ps-summary} - \href{mailto:rrueger@ethz.ch}{rrueger@ethz.ch}}
    \fancyfoot[LE,RO]{\thepage}
---

# 6 Standard Discrete Distributions

## 6.1 Introduction to Special Distributions

We begin with some examples

### The Discrete Uniform Distribution
Is a distribution given by the pmf
\begin{align*}
  p_{\mathrm{uni}}(x) = \frac{1}{n}
\end{align*}
For a random variable corresponding to a *uniform* distribution we write $X \sim \mathrm{Unif}\{1, 2, 3, \ldots\}$.

### The Binomial Distribution
Is a distribution given by the pmf
\begin{align*}
  p_{\mathrm{bin}}(x) = { n \choose x } p^x (1-p)^{n-x} \quad \text{ for } n = 1, 2, 3, \ldots
\end{align*}
For a random variable corresponding to a *binomial* distribution we write $X \sim \mathrm{Bin}(n, p)$.

### The geometric Distribution
Is a distribution given by the pmf
\begin{align*}
  p_{\mathrm{geo}}(x) = p (1-p) ^{x-1} \quad \text{ for } x = 1, 2, 3, \ldots
\end{align*}
For a random variable corresponding to a *geometric* distribution we write $X \sim \mathrm{Geo}(p)$.

### The Negative Binomial Distribution
Is a distribution given by the pmf
\begin{align*}
  p_{\mathrm{nbin}}(x) = { {x-1} \choose {r-1} } p^r (1-p)^{r-x} \quad \text{ for } x = r, r + 1, r+ 2, \ldots
\end{align*}
For a random variable corresponding to a *negative* binomial distribution we write $X \sim \mathrm{NB}(r, p)$.

### The hypergeometric Distribution
Is a distribution given by the pmf
\begin{align*}
  p_{\mathrm{hgeo}}(x) = \frac{ {D \choose x } { {N-D} \choose {n-x} }}{{N \choose n}} \quad \text{ for } n - N + D \leq x \leq D
\end{align*}
For a random variable corresponding to a *hypergeometric* distribution we write $X \sim \mathrm{HyperGeo}(n, D, N)$.

### The Poisson Distribution
Is a distribution given by the pmf
\begin{align*}
  p_{\mathrm{poi}} = \frac{e^{-\lambda} \lambda ^x}{x!} \quad \text{ for } x = 0, 1, 2, \ldots
\end{align*}
For a random variable corresponding to a *poisson* distribution we write $X \sim \mathrm{P}(\lambda)$.

## 6.2 Discrete Uniform Distribution

### The CDF
**Lemma.** The CDF of a uniform distribution on $n$ objects is given by
\begin{align*}
  F(k) = P(X \leq k) = \frac{k}{n}
\end{align*}

### Moments
**Calculation.**
Moments of the uniform distribution are
\begin{gather*}
  E(X) = \sum _{x=1} ^n xp(X) = \sum _{x=1} ^n x\frac{1}{n} = \frac{1}{n} \sum _{x=1} ^n x = \frac{1}{n} \frac{n(n+1)}{2} = \frac{n+1}{2} \quad (~=\mu~) \\
  E(X^2) = \sum _{x=1} ^n x^2 p(x) = \frac{1}{n} \sum _{x=1} ^n x^2 = \frac{1}{n} \frac{n(n+1)(2n+1)}{6} = \frac{(n+1)(2n+1)}{6} \\
  E(X^3) = 0
\end{gather*}

### Variance
**Corollary.** By calculating the moments, we have
\begin{align*}
  \sigma ^2 = \mathrm{Var}(X) = E(X^2) - E(X)^2 = \frac{(n+1)(2n+1)}{6} - \frac{(n+1)^2}{4} = \frac{n^2 - 1}{12}
\end{align*}

**Theorem.** Let $X \sim \mathrm{Unif}\{1, \ldots, n\}$. Then
\begin{align*}
  E((X-\mu)^4) = \frac{(3n^2 -7)(n^2-1)}{240}
\end{align*}

**Corollary.** The skewness and kurtosis of the discrete uniform distribution are given by
\begin{gather*}
  \beta = 0 \\
  \gamma = - \frac{6}{5} \frac{n^2 +1}{n^2-1}
\end{gather*}

## 6.3 The Binomial Distribution

*Remark.* The binomial distribution with $p=\frac{1}{2}$ is symmetric around $n/2$.

**Theorem.** Let $X \sim \mathrm{Bin}(n, p)$. Then

(i) $\mu = E(X) = np$
(ii) $\sigma ^2 = \mathrm{Var}(X) = np(1-p)$
(iii) $E((X-\mu)^3) = np(1-3p+2p^2)$
(iv) $E((X-\mu)^4) = np(1-p) \left (1+ 3p(n-2)(1-p) \right )$

**Corollary.** The skewness and the kurtosis of a binomially distributed variable tend to infinity for large $n$.

## 6.4 Geometric and Negative Binomial Distributions

**Theorem.** (The Geometric Distribution has no memory) Let $X \sim \mathrm{Geo}(p)$ and $m, \in \mathbb{N}$, then
\begin{align*}
  P(X > m+n | X>n) = P(X>m)
\end{align*}

### Expectation and Variance
**Theorem.**

Let $X \sim \mathrm{Geo}(p)$. Then
\begin{gather*}
  E(X) = \frac{1}{p} \\
  \mathrm{Var}(X) = \frac{1-p}{p^2}
\end{gather*}

**Theorem.**

Let $X \sim \mathrm{NB}(r, p), r \geq 1$. Then

\begin{gather*}
  E(X) = \frac{r}{p} \\ \\
  \mathrm{Var}(X) = r \cdot \frac{1-p}{p^2}
\end{gather*}

\newpage
## Insert: Overview
\renewcommand{\arraystretch}{3}
\begin{centering}
  \begin{tabular}{| c | c | c | c | c | c |}

    \hline

    Distribution & Notation & PMF $p(x)$ & CDF $F(k)$ & Expectation & Variance \\

    \hline

    Uniform & $\mathrm{Unif} \{1, \ldots, n\} $ & $\frac{1}{n}$ & $\frac{k}{n}$ & $\frac{n+1}{2}$  & \\
    Binomial & $\mathrm{Bin}(n, p)$ & $np$ & $\sum _{i=0} ^{\lceil k \rceil} { n \choose i } p^i (1-p)^{n-i} $ & $np(1-p)$  & \\
    Geometric & $\mathrm{Geo}(p)$ & ${ {x-1} \choose {r-1} } p^r (1-p)^{r-x}$ & & $\frac{1}{p}$  & \\
    Negative Binomial & $\mathrm{NB}(r, p)$ & ${ {k + r -1 } \choose k} p^k (1-p)^r$ &  & $r\frac{p}{1-p}$  & $r\frac{p}{(1-p)^2}$\\
    Hyper Geometric & $\mathrm{HyperGeo}(n, D, N)$ & $\frac{ {D \choose x } { {N-D} \choose {n-x} }}{{N \choose n}}$ & & & \\
    Poisson & $\mathrm{P}(\lambda)$ & $\frac{e^{-\lambda} \lambda ^x}{x!}$ & & $\lambda$ & $\lambda$ \\

    \hline

  \end{tabular}
\end{centering}
