---
geometry: margin=1in
header-includes: |
    \usepackage{dsfont}
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \renewcommand{\footrulewidth}{0.4pt}
    \fancyhead[CO,CE]{ETH Z\"urich - Probability and Statistics - Dr. F. Balabdaoui}
    \fancyhead[RO,RE]{}
    \fancyhead[LO,LE]{}
    \fancyfoot[CO,CE]{\href{https://gitlab.ethz.ch/rrueger/ps-summary}{git.ethz.ch/rrueger/ps-summary} - \href{mailto:rrueger@ethz.ch}{rrueger@ethz.ch}}
    \fancyfoot[LE,RO]{\thepage}
---

# 4 Integer Valued and Discrete Random Variables

## 4.1 Mass Functions

**Definition.** (Random Variable) Let $\Omega$ be the sample space of some given experiment. We call a function $X{:}\, \Omega \to \mathbb{R}$ a **random variable**.

**Defintion.** (Probability mass function, PMF) Let $X$ be a discrete random variable, taking a countable number of values $(x_n)_n \subseteq \mathbb{R}$. The function
\begin{align*}
  p(x){:}\, \mathbb{R} \to \mathbb{R}, x \mapsto
  \begin{cases}
    P(X = x) & x \in (x_n)_n \\
    0 & \text{else}
  \end{cases}
\end{align*}
is the **probability mass function** (pmf) of $X$.

## 4.2 CDF and Median of a Random Variable

**Definition.** (Cumulative distribution function, CDF) The cumulative distribution function of a random variable $X$ is the function $F(x){:}\, \mathbb{R} \to \mathbb{R}, x \mapsto P(X \leq x)$.

**Definition.** (Median) For $F$ the CDF of some random variable $X$, a number $m \in \mathbb{R}$ satisfying $P(X \leq m) \geq \frac{1}{2}$ *and* $P(X \geq m) \geq \frac{1}{2}$ is called the **median** of $F$.

*Remark.* The median need *not* be unique.

**Theorem.** A function $F$ is the CDF of some real-valued random variable $X$ if and only if the following hold

(a) $\forall x \in \mathbb{R}: F(x) \in [0,1]$
(b) $\lim _{x \to - \infty} F(x) = 0$ and $\lim _{x \to \infty} F(x) = 1$
(c) *(Right continuous)* $\lim _{x \searrow a} F(x) = F(a)$.
(d) *(Monotonous)* $\forall x \leq y \in \mathbb{R}: F(x) \leq F(y)$

**Proposition.** For $F$ the CDF of some random variable $X$, it holds for all $x \in \mathbb{R}$

(a) $P(X = x) = F(x) - \lim _{y \nearrow x} F(y) = F(x) - F(x_{-})$
(b) $P(X \geq x) = P(X > x) + P(X = x) = (1 - F(x)) + (F(x) - F(x_{-})) = 1 - F(x_{-})$

**Definition.** (Indicator random variable) An **indicator random variable** for an event $A$ is one that is $1$ if $A$ occurs and $0$ otherwise.

*Remark.* In the context of measure theory, an indicator random variable is no more than the indicator function on the sample space for the set that represents the event $A$.

### 4.2.1 Functions of Random Variables

**Proposition.** (Function of a Random Variable) For $X$ a random variable, and $Y = g(X)$ for some real-valued function $g$, we have
\begin{align*}
  P(Y = y) = \sum _{x \in \mathbb{R}: g(x) = y} p(x)
\end{align*}

**Proposition.** Let $X$ be a random variable. If the distribution of $X$ is even (i.e. $P(X = x) = P(X = -x)$), then any random variable that is an odd function of $X$ also has an even distribution.

### 4.2.2 Independence of Random Variables

**Definition.** (Independent) A set of discrete random variables $X_1, \ldots, X_k$ on some sample space $\Omega$ are called **independent** if
\begin{align*}
  P(X_1 = x_1, \ldots, X_k = x_k) = \prod _{j=1} ^k P(X_j = x_j)
\end{align*}

**Theorem.** Let $R = \{X_1, \ldots, X_k\}$ be a set of discrete independent random variables. Any functions of these variables with disjoint sets of arguments are independent. More formally: let $I_1, \ldots, I_j$ be pairwise disjoint subsets of $R$, then $Y_1 = f_1(I_1), \ldots, Y_j = f_j(I_j)$ are also independent random variables.

**Definition.**i (Independent and identically distributed random variables, IID) We call a set of independent variables $X_1, \ldots, X_k$ with the same CDF $F$ **independent and identically distributed random variables** and write
\begin{align*}
  X_1, \ldots, X_k \overset{\mathrm{iid}}{\sim} F.
\end{align*}

## 4.3 The Expected Value of a Discrete Random Variable

**Definition.** (Expected Value) We say that the **expected value** of a discrete random variable $X$ exists if
\begin{align*}
  \sum _i |x_i| p(x_i) < \infty
\end{align*}
In that case, we define the expected value to be
\begin{align*}
  \mu = E(X) = \sum _i x_i p(x_i)
\end{align*}

*Remark.* The qualification about absolute convergence of the sum is important, as Riemann's re-ordering theorem would then allow a potentially conditionally convergent sum to be any arbitrary number, leaving the expected value not being properly defined.

**Proposition.** (Change of variable formula) For an experiment in which the sample space $\Omega$ is countable, then a random variable $X$ has expectation
\begin{align*}
  \mu = \sum _x xp(x) = \sum _\omega X(\omega) P(\omega)
\end{align*}
Where $P(\omega)$ is the probability of the sample point $\omega$.

**Definition.** (Function of several random variables) Let $X_1, \ldots, X_n$ be a set of random variables on a common countable sample space $\Omega$. The expectation of some function $g$ of these random variables exists if the sum $\sum _\omega |g(X_1, \ldots, X_n)| P(\omega)$ is finite. Then we define the **expectation** of $g$ to be
\begin{align*}
  E(g(X_1, \ldots, X_n)) = \sum _\omega g(X_1, \ldots, X_n) P(\omega)
\end{align*}

## 4.4 Basic properties of Expectations

**Proposition.**

(a) If there is a $c$ such that $P(X = c) = 1$, then $E(X) = c$.
(b) For two random variables $X, Y$ on some sample space $\Omega$ it holds
\begin{align*}
  P(X \leq Y) = 1 \implies E(X) \leq E(Y)
\end{align*}
(c) For $X$ with a finite expectation, we have
\begin{align*}
  \begin{cases}
    P(X \geq c) = 1 \implies E(X) \geq c \\
    P(X \leq c) = 1 \implies E(X) \geq c \\
  \end{cases}
\end{align*}

**Proposition.** (Linearity of Expectations) For a set of random variables $X_1, \ldots, X_n$ whose expectations $E(X_1), \ldots, E(X_n)$ are bounded (exist), we have
\begin{align*}
  E \left ( \sum _{i=1} ^n c_i X_i \right ) = \sum _{i=1} ^n c_i E(X_i)
\end{align*}
for any constants $c_1, \ldots, c_n \in \mathbb{R}$.

**Corollary.** The expectation is a linear function.

**Theorem.** The expectation of the product of finitely many *independent* random variables is the product of the expectations, provided they exist.
\begin{align*}
  E \left ( \prod _{i=1} ^n X_i \right ) = \prod _{i=1} ^n E(X_i)
\end{align*}

## 4.6 Using indicator Variables to Calculate Expectations

**Proposition.** Let $X$ be an integer-valued random variable which can be written as the sum $X = \sum _{i=1} ^n c_i \mathds{1}_{A_i}$ for some events $A_i$ and constants $c_i$. The expectation of $X$ is given by
\begin{align*}
  E(X) = E \left (\sum _{i=1} ^n c_i \mathds{1}_{A_i} \right ) \overset{\mathrm{linearity}}{=} \sum _{i=1} ^n c_1 E(\mathds{1}_{A_i}) = \sum _{i=1} ^n c_i P(A_i)
\end{align*}

## 4.7 The Tail Sum Method for Calculating Expectations

**Theorem.** Let $X$ be a random variable taking the values $0, 1, 2, \ldots$. Then

\begin{align*}
  E(X) = \sum _{n=0} ^\infty P(X > n)
\end{align*}

## 4.8 Variance, Moments and Basic Inequalities

**Definition.** (Variance, Standard Deviation) A random variable $X$ with a finite mean $\mu$ has a **variance**
\begin{align*}
  \sigma ^2 = E((X - \mu)^2)
\end{align*}
the *standard deviation* is given by $\sigma := \sqrt{\sigma ^2}$.

*Remark.* The unit of the standard deviation is the same as the unit of the random variable.

**Lemma.** The variance is finite if and only if the expected value of the square is finite.
\begin{align*}
  \sigma ^2 < \infty \implies E(X^2) < \infty
\end{align*}

**Proposition.** For a random variable $X$ we have
\begin{align*}
  \sigma \geq x E(|X - \mu |).
\end{align*}
This inequality is strict, unless $X$ is constant, as then we have $P(X = \mu) = 1$.


**Proposition.**

* $\forall \alpha \in \mathbb{R}: \mathrm{Var}(\alpha X) = \alpha ^2 \mathrm{Var}(X)$.
* $\forall \alpha \in \mathbb{R}: \mathrm{Var}(\alpha + X) = \mathrm{Var}(X)$.
* $\mathrm{Var}(X) \geq 0$.
* $\mathrm{Var}(X) = 0 \iff \exists c \in \mathbb{R}: P(X = c) = 1$.
* $\mathrm{Var}(X) = E(X^2) - \mu ^2$.

**Definition.** (Moment) We call $E(X^k)$, the **k-th moment** of $X$.

*Remark.* Moments can be used to make judgements about the symmetry of a distribution. In particular odd moments ($k > 1$), if they exist, vanish on symmetric distributions. The converse need not be true.

**Definition.** (Skewness, Kurtosis) Let $X$ be random variable.

(a) If $E(|X|^3) < \infty$, then we define the **skewness** of $X$ to be
\begin{align*}
  \beta = \frac{E((X-\mu)^3)}{\sigma ^3}
\end{align*}

(b) If $E(X^4) < \infty$, then we define the **kurtosis** of $X$ to be
\begin{align*}
  \gamma = \frac{E((X-\mu)^4)}{\sigma ^4} - 3
\end{align*}

*Ramark.* The kurtosis is necessarily $\gamma \geq -2$.

## 4.9 Illustrative Examples

### 4.9.1 Variance of a sum of independent Random variables

**Theorem.** Let $X_1, \ldots, X_n$, be independent random variables. Then
\begin{align*}
  \mathrm{Var}(X_1 + \cdots + X_n) = \mathrm{Var}(X_1) + \cdots + \mathrm{Var}(X_n)
\end{align*}

*Remark.* (Notation) We write $\overline{X} = \frac{1}{n} (X_1, \ldots, X_n)$.

**Corollary.** For $X_1, \ldots, X_n$ independent random variables with a common variance $\sigma ^2 < \infty$, then $\mathrm{Var}(\overline{X}) = \frac{\sigma ^2}{n}$.

## 4.10 Utility of $\mu$ and $\sigma$ as Summaries of data

### 4.10.1 Chebyshev's Inequality and the Weak Law of Large Numbers

**Theorem.** (Chebyshev's Inequality) Let $X$ be a random variable with finite expectation and variance. Then for any $k \in \mathbb{N}$
\begin{align*}
  P(|X - \mu| \geq k \sigma) \leq \frac{1}{k^2}
\end{align*}

**Theorem.** (Markov's Inequality) Let $X$ be a non-negative random variable with a finite mean. For $c$ positive we have
\begin{align*}
  P(X \geq c) \leq \frac{\mu}{c}
\end{align*}

**Theorem.** (Weak law of large numbers) Let $X_1, \ldots, X_n$ be iid random variables with a mean $\mu$ and a variation $\sigma ^2 < \infty$. Then
\begin{align*}
  \forall \varepsilon > 0: \lim _{n \to \infty} P(|\overline{X} - \mu |) = 0
\end{align*}

## 4.11 Other Fundamental Moment Inequalities

**Theorem.** (Cauchy-Schwarz Inequality) Let $X$ and $Y$ be random variables with bounded second moments, then
\begin{align*}
  E(|XY|) \leq \sqrt{E(Y^2)}\sqrt{E(X^2)}
\end{align*}

**Theorem.** (Holder's Inequality) Let $X$, $Y$ for which a $p \geq 1 \in \mathbb{R}$ it holds that $E(|X|^p)$, and $E(|Y|^p)$ are finite. With $q := \frac{p}{p-1}$ and $E(|Y|^q)$ finite, then
\begin{align*}
  E(|XY|) \leq E(|X|^p)^{\frac{1}{p}} E(|Y|^q)^{\frac{1}{q}}
\end{align*}
