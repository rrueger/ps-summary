\thispagestyle{empty}
\begin{center}

  \scshape Eidgenössische Technische Hochschule Zürich\normalfont\\[2.5cm]

  \rule{\linewidth}{0.5mm}%
  \\[0.4cm]

  {\Large\bfseries Probability and Statistics\\[0.4cm] }

  \rule{\linewidth}{0.5mm}%
  \\[1.5cm]

  \small Summer Semester 2019 \\[1cm]

  Summary of the lectures by \textsc{Dr. F. Balabdaoui} written by Ryan Rueger

\end{center}
\vfill
\newpage
\tableofcontents
\newpage
